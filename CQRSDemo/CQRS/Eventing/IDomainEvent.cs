﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CQRS.Eventing
{
    public interface IDomainEvent: IMessage
    {
        long AggregateRootId { get; set; }
    }
}
