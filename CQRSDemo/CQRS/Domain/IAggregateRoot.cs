﻿using CQRS.Eventing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CQRS.Domain
{
    public interface IAggregateRoot
    {
        string UniqueId { get; }

        int Version { get; }

        IEnumerable<IDomainEvent> GetChanges();

        void AcceptChanges(int newVersion);

        void ReplayEvents(IEnumerable<DomainEventStream> eventStreams);

    }

}
