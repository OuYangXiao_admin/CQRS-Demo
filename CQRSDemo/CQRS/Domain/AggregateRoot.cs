﻿using CQRS.Eventing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CQRS.Domain
{

    [Serializable]
    public abstract class AggregateRoot : IAggregateRoot
    {
        private static readonly IList<IDomainEvent> _emptyEvents = new List<IDomainEvent>();
        private static IAggregateRootInternalHandlerProvider _eventHandlerProvider;
        private int _version;
        private Queue<IDomainEvent> _uncommittedEvents;
        protected long _id;

        public long Id
        {
            get { return _id; }
            protected set { _id = value; }
        }

        protected AggregateRoot()
        {
            _uncommittedEvents = new Queue<IDomainEvent>();
            //_eventHandlerProvider = 
        }
        protected AggregateRoot(long id) : this()
        {
            if (id < default(double))
            {
                throw new ArgumentNullException("id");
            }
            _id = id;
        }

        protected AggregateRoot(long id, int version) : this(id)
        {
            if (version < 0)
            {
                throw new ArgumentException(string.Format("version 不能小于零, aggregateRootId: {0}, version: {1}", id, version));
            }
            _version = version;
        }

        protected void ApplyEvent(IDomainEvent domainEvent)
        {
            if (domainEvent == null)
            {
                throw new ArgumentNullException("domainEvent");
            }
            if (Equals(this._id, default(long)))
            {
                throw new Exception("Aggregate root id cannot be null.");
            }
            domainEvent.AggregateRootId = _id;
            domainEvent.Version = _version + 1;
            HandleEvent(domainEvent);
            AppendUncommittedEvent(domainEvent);
        }

        protected void ApplyEvents(params IDomainEvent[] domainEvents)
        {
            foreach (var domainEvent in domainEvents)
            {
                ApplyEvent(domainEvent);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="domainEvent"></param>
        private void HandleEvent(IDomainEvent domainEvent)
        {
            if (_eventHandlerProvider == null)
            {
                _eventHandlerProvider = IocContainer.Resolve<IAggregateRootInternalHandlerProvider>();
            }
            var handler = _eventHandlerProvider.GetInternalEventHandler(GetType(), domainEvent.GetType());
            if (handler == null)
            {
                throw new Exception(string.Format("Could not find event handler for [{0}] of [{1}]", domainEvent.GetType().FullName, GetType().FullName));
            }
            if (Equals(this._id, default(long)) && domainEvent.Version == 1)
            {
                this._id = TypeUtils.ConvertType<long>(domainEvent.AggregateRootStringId);
            }
            handler(this, domainEvent);
        }

        private void AppendUncommittedEvent(IDomainEvent domainEvent)
        {
            if (_uncommittedEvents == null)
            {
                _uncommittedEvents = new Queue<IDomainEvent>();
            }
            if (_uncommittedEvents.Any(x => x.GetType() == domainEvent.GetType()))
            {
                throw new InvalidOperationException(string.Format("事件的领域类型必须一致: {0}, current aggregateRoot type: {1}, id: {2}", domainEvent.GetType().FullName, this.GetType().FullName, _id));
            }
            _uncommittedEvents.Enqueue(domainEvent);
        }
        private void VerifyEvent(DomainEventStream eventStream)
        {
            var current = this as IAggregateRoot;
            if (eventStream.Version > 1 && eventStream.AggregateRootId != current.UniqueId)
            {
                throw new InvalidOperationException(string.Format("错误的领域事件流, aggregateRootId:{0}, expected aggregateRootId:{1}, type:{2}", eventStream.AggregateRootId, current.UniqueId, current.GetType().FullName));
            }
            if (eventStream.Version != current.Version + 1)
            {
                throw new InvalidOperationException(string.Format("错误的领域事件流, version:{0}, expected version:{1}, current aggregateRoot type:{2}, id:{3}", eventStream.Version, current.Version, this.GetType().FullName, current.UniqueId));
            }
        }

        string IAggregateRoot.UniqueId
        {
            get
            {
                if (Id > 0)
                {
                    return Id.ToString();
                }
                return null;
            }
        }
        int IAggregateRoot.Version
        {
            get { return _version; }
        }
        IEnumerable<IDomainEvent> IAggregateRoot.GetChanges()
        {
            if (_uncommittedEvents == null)
            {
                return _emptyEvents;
            }
            return _uncommittedEvents.ToArray();
        }
        void IAggregateRoot.AcceptChanges(int newVersion)
        {
            if (_version + 1 != newVersion)
            {
                throw new InvalidOperationException(string.Format("无效的版本: {0}, expect version: {1}, current aggregateRoot type: {2}, id: {3}", newVersion, _version + 1, this.GetType().FullName, _id));
            }
            _version = newVersion;
            _uncommittedEvents.Clear();
        }
        void IAggregateRoot.ReplayEvents(IEnumerable<DomainEventStream> eventStreams)
        {
            if (eventStreams == null) return;

            foreach (var eventStream in eventStreams)
            {
                VerifyEvent(eventStream);
                foreach (var domainEvent in eventStream.Events)
                {
                    HandleEvent(domainEvent);
                }
                _version = eventStream.Version;
            }
        }

    }
}
