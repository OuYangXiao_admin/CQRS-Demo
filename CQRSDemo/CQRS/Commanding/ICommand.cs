﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CQRS.Commanding
{
    public interface ICommand : IMessage
    {
        string AggregateRootId { get; }
    }

    [Serializable]
    public abstract class Command : Message, ICommand
    {
        public string AggregateRootId { get; set; }

      
        public Command() : base() { }
     
        public Command(string aggregateRootId)
        {
            if (aggregateRootId == null)
            {
                throw new ArgumentNullException("aggregateRootId");
            }
            AggregateRootId = aggregateRootId;
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        public override string GetRoutingKey()
        {
            return AggregateRootId;
        }
    }
}
