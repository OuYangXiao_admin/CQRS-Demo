﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CQRS
{
    public interface IMessage
    {
        string Id { get; set; }
        DateTime Timestamp { get; set; }

        int Sequence { get; set; }

        string GetRoutingKey();

        string GetTypeName();

        string AggregateRootStringId { get; set; }

        string AggregateRootTypeName { get; set; }

        int Version { get; set; }

    }

    public abstract class Message : IMessage
    {
        public string AggregateRootStringId
        {
            get;set;
        }

        public string AggregateRootTypeName
        {
           get; set;
        }

        public string Id
        {
            get; set;
        }

        public int Sequence
        {
            get; set;
        }

        public DateTime Timestamp
        {
            get; set;
        }

        public int Version
        {
            get; set;
        }

        public virtual string GetRoutingKey()
        {
            return null;
        }

        public string GetTypeName()
        {
            return this.GetType().FullName;
        }
    }
}
